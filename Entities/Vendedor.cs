using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public class Vendedor
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="O nome é obrigatório")]
        [MinLength(11,ErrorMessage ="O CPF deve ter no mínimo 11 números")]
        [MaxLength(11,ErrorMessage ="O CPF deve ter no máximo 11 números")]
        public string Cpf { get; set; }

        [Required(ErrorMessage ="O email é obrigatório")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage ="Digite um email válido")]
        public string Email { get; set; }

        [Required(ErrorMessage ="O nome é obrigatório")]
        [MinLength(3,ErrorMessage ="O nome deve ter no mínimo 3 letras.")]
        public string Nome { get; set; }

        public string Telefone { get; set; }
        public Vendedor(int id, string cpf, string nome, string email, string numeroTelefone)
        {
            Id = id;
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = numeroTelefone;
        }
    }
}