using System.Runtime.Serialization;

namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        [EnumMember(Value ="Aguardando Pagamento")]
        AguardandoPagamento,
        [EnumMember(Value ="Pagamento Aprovado")]
        PagamentoAprovado,
        [EnumMember(Value ="Enviado pra transportadora")]
        EnviadoPraTransportadora,
        [EnumMember(Value ="Entregue")]
        Entregue,
        [EnumMember(Value ="Cancelada")]
        Cancelada
    }
}