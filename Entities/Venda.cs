using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;




namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public DateTime dataVenda { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumStatusVenda Status { get; set; }
        public int Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<ItensVenda> ItensDaVenda = new();
        
    public Venda(int id, Vendedor vendedor)
    {
        Id= id;
        dataVenda = DateTime.Now;
        Vendedor=vendedor;
        Status = EnumStatusVenda.AguardandoPagamento;
    }

    public void AddProduct (ItensVenda itemvenda)
    {
        ItensDaVenda.Add(itemvenda);
    }

    public string Serialized (Formatting formating = Formatting.Indented)
    {
        return JsonConvert.SerializeObject(this, formating);
    }



    }




}