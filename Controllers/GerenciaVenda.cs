using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    public class GerenciaVenda
    {
        private List<Venda> ListaVenda = new();
        public List<Venda> RetornaListaVenda()
        {
            return ListaVenda;
        }

        public Venda CriaVenda(
            int vendaId, int idUsuario, string cpfUsuario, string nomeUsuario, string emailUsuario, string numeroTelefoneUsuario, List<ItensVenda>item
            )
        {
            Venda venda = new Venda(vendaId, new Vendedor(idUsuario, cpfUsuario,nomeUsuario,emailUsuario, numeroTelefoneUsuario));

            AdicionaVendaALista(venda);
            AdicionaProdutosAVenda(venda, item);
            return venda;
        }
        private void AdicionaVendaALista(Venda venda)
        {
            ListaVenda.Add(venda);
        }

        private void AdicionaProdutosAVenda(Venda venda, List<ItensVenda> itens)
        {
            foreach (var item in itens)
            {
                venda.AddProduct(new ItensVenda(item.Nome));
            }
        }

        public Venda AtualizaStatusVenda(Venda venda, EnumStatusVenda statusVenda)
        {
            if(venda.Status == EnumStatusVenda.AguardandoPagamento && (statusVenda == EnumStatusVenda.PagamentoAprovado || statusVenda == EnumStatusVenda.Cancelada))
            {
                venda.Status = statusVenda;
            }

            else if (venda.Status == EnumStatusVenda.PagamentoAprovado && (statusVenda == EnumStatusVenda.EnviadoPraTransportadora || statusVenda == EnumStatusVenda.Cancelada))
            {
                venda.Status = statusVenda;
            }

            else if (venda.Status == EnumStatusVenda.EnviadoPraTransportadora && statusVenda== EnumStatusVenda.Entregue)
            {
                venda.Status = statusVenda;
            }

            return venda;
        }
    }
}