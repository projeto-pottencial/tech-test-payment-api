using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{   [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly GerenciaVenda _venda;

        public VendaController(IOptions<GerenciaVenda>vendaOptions)
        {
            _venda = vendaOptions.Value;
        }

        [HttpPost("RegistraVenda")]
        public IActionResult CriaVenda(int vendaId, int idUsuario, string cpfUsuario, string nomeUsuario, string emailUsuario, string numeroTelefoneUsuario, List<ItensVenda> item)
        {
            if(item.Count<= 0)
            {
                return BadRequest(new{message = "Uma venda deve conter ao menos um item."});
            }

            Venda venda = _venda.CriaVenda(vendaId,idUsuario, cpfUsuario,nomeUsuario,emailUsuario,numeroTelefoneUsuario, item);
        
            return Ok(venda.Serialized());
        }

        [HttpGet("RetornaVendaPorId")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _venda.RetornaListaVenda().Find(x => x.Id == id);

            if (venda == null)
                return NotFound();
            
            return Ok(venda.Serialized());
        }

        public IActionResult AtualizaStatusVenda(int vendaId, EnumStatusVenda statusVenda)
        {
            var venda = _venda.RetornaListaVenda().Find(x=> x.Id == vendaId);

            if (venda == null)
             return NotFound();

            _venda.AtualizaStatusVenda(venda, statusVenda);

            return Ok(venda.Serialized());
        }


    }
}